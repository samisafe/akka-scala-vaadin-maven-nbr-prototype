import sbt._
import sbt.Keys._

object Dependencies {

  val vaadinVersion = "7.5.3"

  val vaadinServer = "com.vaadin" % "vaadin-server" % vaadinVersion
  val vaadinClient = "com.vaadin" % "vaadin-client" % vaadinVersion
  val vaadinClientCompiler = "com.vaadin" % "vaadin-client-compiler" % vaadinVersion
  val vaadinThemes = "com.vaadin" % "vaadin-themes" % vaadinVersion

  val addonDeps = Seq(
    vaadinServer,
    vaadinClient
  )

  val actualDeps = Seq(
    vaadinClientCompiler % "provided",
    vaadinThemes
  )

}