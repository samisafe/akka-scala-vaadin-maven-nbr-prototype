Akka-Scala-Vaadin-Maven-NBR-Prototype
==============

A Vaadin app for NBR project with Scala, Akka and Maven technologies


Workflow (Maven)
========

  * [] `$ mvn install`
    To compile the entire project, run "mvn install".
  * [] `$ mvn jetty:run`
    To run the application. Then open http://localhost:8080/
    - Use `http://localhost:8080/?Debug` to debug.

  * [] `$ mvn vaadin:run-codeserver`
    For Debugging client side code
    - run "mvn vaadin:run-codeserver" on a separate console while the application is running
    - activate Super Dev Mode in the debug window of the application

  * [] `$ mvn clean vaadin:compile-theme package`
    To produce a deployable production mode WAR:
    - change productionMode to true in the servlet class configuration (nested in the UI class)
    - run "mvn clean vaadin:compile-theme package"
    - See below for more information. Running "mvn clean" removes the pre-compiled theme.
  * [] `$ mvn jetty:run-war`
    - For testing
  * [] Sometimes, `$ mvn clean vaadin:compile` is healthy.

Using a precompiled theme
-------------------------

When developing the application, Vaadin can compile the theme on the fly when needed,
or the theme can be precompiled to speed up page loads.

 * [] `$ mvn vaadin:compile-theme` 
    To precompile the theme run "mvn vaadin:compile-theme". Note, though, that once
    the theme has been precompiled, any theme changes will not be visible until the
    next theme compilation or running the "mvn clean" target.

** When developing the theme, running the application in the "run" mode (rather than
   in "debug") in the IDE can speed up consecutive on-the-fly theme compilations
   significantly.

** Resources: https://vaadin.com/maven#start




Workflow (Gradle)
========

A - Running the application using Vaadin Plugin 4 Gradle
--

I am still trying to figure out how to solve the missing widgetset problem

  * [1] `$ gradle clean vaadinRun`
  * [0] `$ gradle clean vaadinCompileWidgetset`
    To clean and compile widgetset; Then, pursue [1]. 

B - Running the application using Gradle Gretty Plugin

  * [1] `$ gradle appRun`
      Open http://localhost:8080/ASV-NBR-prototype/
  * [0] `$ gradle clean vaadinCompileWidgetset`
    To clean and compile widgetset; Then, pursue [1]. 
 
 
2-Do List
==

* [IN-PROG]   (1) Provide SBT scala build file
* [PART-DONE] (2) Provide gradle build file
* (3) Polish GUI

