package org.vaadin.maven.scala.akka.protoytype.view

import java.util.Date

import com.vaadin.ui.AbstractLayout
import com.vaadin.ui.Label
import com.vaadin.ui.VerticalLayout

class UserStoryHeader(component: AbstractScalaPrototypeComponent)
    extends AbstractScalaPrototypeComponent(component, new VerticalLayout) {

  override def defineWithin(container: AbstractLayout): Unit = {
    require(null != container)
    val greetingUser: Label = new Label("Hello " + System.getProperty("user.name") + "!")
    val today = "Logged in Time: " + new Date
    val todayLabel = new Label(today)
    ScalaComponentOpers.addComponents(container, greetingUser, todayLabel)
  }

  override def reset: Unit = ??? //TODO
}