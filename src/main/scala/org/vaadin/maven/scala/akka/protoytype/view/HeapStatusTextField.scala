package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.ui.AbstractOrderedLayout
import org.vaadin.maven.scala.akka.protoytype.foundation.HeapStatusSimpleProvider

final class HeapStatusTextField(container: AbstractOrderedLayout) extends AbstractUpdatableTextField(container) {

  setCaption(composeHeapInfo)
  setValue(HeapStatusSimpleProvider.provideHeapStatus())
  setExpandRatio()

  private def composeHeapInfo: String = {
    val strBldr = new StringBuilder()
    strBldr.append("VM (").append(System.getProperty("java.vm.info")).append("): Heap ")
    return strBldr.toString
  }

  override protected def setExpandRatio: Unit =
    this.getVaadinContainer.setExpandRatio(this.getTextField, 25.0f)
}