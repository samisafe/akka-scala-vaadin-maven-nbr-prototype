package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.ui.AbstractLayout
import com.vaadin.ui.CustomComponent

abstract class AbstractScalaPrototypeComponent(component: AbstractScalaPrototypeComponent, container: AbstractLayout) extends CustomComponent {
  private val parent: AbstractScalaPrototypeComponent = resolveParent(component)
  protected val layout: AbstractLayout = container

  ScalaComponentOpers.establishLayout(this, this.layout)
  setCompositionRoot(this.layout)
  defineWithin(this.layout)

  protected final def getGenuineParent = this.parent

  def reset: Unit

  protected def defineWithin(layout: AbstractLayout): Unit

  private def resolveParent(component: AbstractScalaPrototypeComponent): AbstractScalaPrototypeComponent = component match {
    case null          => this
    case NullUserStory => this
    case _             => component
  }

  trait PrototypeComponentVisitor {
    def visitPrototypeComponent(component: AbstractScalaPrototypeComponent)
  }

  def accept(visitor: PrototypeComponentVisitor) = {
    require(null != visitor)
    visitor.visitPrototypeComponent(this)
  }
}