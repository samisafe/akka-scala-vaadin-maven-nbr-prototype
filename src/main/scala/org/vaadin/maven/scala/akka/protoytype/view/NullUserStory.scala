package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.ui.AbstractLayout

case object NullUserStory extends AbstractUserStory("Null User Story") {

  protected def defineWithin(layout: AbstractLayout) = setSizeFull()

  override def toString: String = return this.getCaption
}