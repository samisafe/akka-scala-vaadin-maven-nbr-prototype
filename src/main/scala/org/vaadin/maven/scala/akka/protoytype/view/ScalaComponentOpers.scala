package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.shared.ui.MarginInfo
import com.vaadin.ui.AbstractLayout
import com.vaadin.ui.AbstractOrderedLayout
import com.vaadin.ui.Component
import com.vaadin.ui.GridLayout

object ScalaComponentOpers {
  def addComponents(layout: AbstractLayout, components: Component*): Unit = {
    if (null == layout || null == components) {
      //TODO : java code -> write it in Scala
      //  LoggerFactory.getLogger(ComponentOperations.class).warn("'layout' and 'components' both should not be null")
      return
    }
    components.foreach { x => layout.addComponent(x) }
  }

  def establishLayout(component: AbstractScalaPrototypeComponent, layout: AbstractLayout): Unit = {
    require(null != component)
    require(null != layout)
    layout.setSizeFull()
    if (layout.isInstanceOf[GridLayout]) {
      layout.asInstanceOf[GridLayout].setSpacing(true)
      layout.asInstanceOf[GridLayout].setMargin(true)
    }
    component.setSizeFull()
    if (layout.isInstanceOf[AbstractOrderedLayout]) {
      layout.asInstanceOf[AbstractOrderedLayout].setMargin(new MarginInfo(true, false, false, false))
      layout.asInstanceOf[AbstractOrderedLayout].setSpacing(true)
      component.setSizeUndefined()
    }
  }
}