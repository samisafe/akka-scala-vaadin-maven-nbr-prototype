package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.ui.AbstractComponent
import com.vaadin.ui.CustomComponent
import com.vaadin.ui.GridLayout

abstract class AbstractUserStory(vaadinContainer: AbstractComponent, userStory: AbstractUserStory, cap: String)
    extends AbstractScalaPrototypeComponent(userStory, new GridLayout(1, 4)) {

  private val vaadinParent: AbstractComponent = vaadinContainer
  private val caption: String = cap
  require(null != vaadinContainer)
  require(null != cap && !cap.isEmpty())
  setCaption(this.caption);

  def this(vaadinContainer: AbstractComponent, cap: String) = this(vaadinContainer, NullUserStory, cap)
  def this(cap: String) = this(new CustomComponent, cap)

  override final def reset = this.layout.removeAllComponents
}