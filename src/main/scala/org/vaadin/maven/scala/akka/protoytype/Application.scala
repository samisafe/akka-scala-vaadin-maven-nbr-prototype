package org.vaadin.maven.scala.akka.protoytype

import org.vaadin.maven.scala.akka.protoytype.view.AbstractUserStory
import org.vaadin.maven.scala.akka.protoytype.view.DefaultUserStory
import com.vaadin.annotations.Push
import com.vaadin.annotations.Theme
import com.vaadin.annotations.Widgetset
import com.vaadin.server.VaadinRequest
import com.vaadin.ui.UI
import com.vaadin.ui.VerticalLayout
import com.vaadin.shared.communication.PushMode

@Push(PushMode.AUTOMATIC)
@Theme("mytheme")
@Widgetset("org.vaadin.maven.scala.akka.prototype.MyAppWidgetset")
class Application extends UI {
  override def init(request: VaadinRequest) = {
    val container = new VerticalLayout
    val content: AbstractUserStory = new DefaultUserStory(this)
    setContent(content)
    /**
      * Add-on developers should note that this method is only meant for
      * the application developer.
      * An add-on should not set the poll interval directly, rather
      * instruct the user to set it.
      */
    setPollInterval(11000)
  }
}