package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.ui.AbstractComponent
import com.vaadin.ui.AbstractLayout

final class DefaultUserStory(container: AbstractComponent)
    extends AbstractUserStory(container, "Default User Story") {

  override def defineWithin(container: AbstractLayout) = {
    require(null != container)
    ScalaComponentOpers.addComponents(container, new UserStoryHeader(this), new UserStoryInput(this), new EnvironmentStatusbar(this))
  }
}