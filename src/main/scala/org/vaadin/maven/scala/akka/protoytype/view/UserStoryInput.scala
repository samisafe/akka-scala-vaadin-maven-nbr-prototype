package org.vaadin.maven.scala.akka.protoytype.view

import com.vaadin.shared.ui.MarginInfo
import com.vaadin.ui.AbstractLayout
import com.vaadin.ui.Button
import com.vaadin.ui.Button.ClickEvent
import com.vaadin.ui.Button.ClickListener
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.Notification
import com.vaadin.ui.TextField
import com.vaadin.ui.VerticalLayout

class UserStoryInput(component: AbstractScalaPrototypeComponent)
    extends AbstractScalaPrototypeComponent(component, new VerticalLayout()) {

  override def defineWithin(container: AbstractLayout): Unit =
    {
      require(null != container)
      if (container.isInstanceOf[HorizontalLayout]) {
        container.asInstanceOf[HorizontalLayout].setSpacing(true);
        container.asInstanceOf[HorizontalLayout].setMargin(new MarginInfo(true, false, true, false));
      }
      val txtfIn = new TextField("Enter network backup file:")
      val okBtn = new Button("OK", new ClickListener {
        override def buttonClick(event: ClickEvent) =
          Notification.show("Implementation not provide yet")
      })
      ScalaComponentOpers.addComponents(container, txtfIn, okBtn);
    }

  override def reset: Unit = ???
}