package org.vaadin.maven.scala.akka.protoytype.controller

import scala.concurrent.duration.DurationInt

import org.vaadin.maven.scala.akka.protoytype.core.Updatable
import org.vaadin.maven.scala.akka.protoytype.foundation.HeapStatusSimpleProvider

import akka.actor.Actor
import akka.actor.ActorSystem
import akka.actor.Inbox
import akka.actor.Props
import akka.actor.actorRef2Scala

class HeapStatusUpdateActorModule(target: Updatable) {
  private val updatable = target
  def execute: Unit = {
    val system = ActorSystem("heapstatus")
    val hsua = system.actorOf(Props(new HeapStatusUpdateActor(updatable)), "hsua")
    val inbox = Inbox.create(system)
    inbox.send(hsua, Forwarder)
    system.scheduler.schedule(0.seconds, 10.seconds, hsua, Forwarder)(system.dispatcher, hsua)
  }
}

sealed trait HSMessage
case object Forwarder extends HSMessage
case class Target(target: Updatable, status: String) extends HSMessage

class HeapStatusUpdateActor(target: Updatable) extends Actor {
  private val updatable = target
  override def receive = {
    case Target(target, status) => {
      target.update(status)
      println(status)
    }
    case Forwarder => sender ! Target(target, HeapStatusSimpleProvider.provideHeapStatus())
  }
}