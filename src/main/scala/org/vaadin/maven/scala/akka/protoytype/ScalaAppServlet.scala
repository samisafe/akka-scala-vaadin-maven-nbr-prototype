package org.vaadin.maven.scala.akka.protoytype

import com.vaadin.annotations.VaadinServletConfiguration
import com.vaadin.server.VaadinServlet

import javax.servlet.annotation.WebServlet

@WebServlet(urlPatterns = Array("/*"), name = "AppServlet", asyncSupported = true)
@VaadinServletConfiguration(ui = classOf[Application], productionMode = false)
class ScalaAppServlet extends VaadinServlet{
  
}