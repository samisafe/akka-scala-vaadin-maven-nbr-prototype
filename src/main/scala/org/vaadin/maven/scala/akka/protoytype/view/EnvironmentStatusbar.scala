package org.vaadin.maven.scala.akka.protoytype.view

import org.vaadin.maven.scala.akka.protoytype.controller.HeapStatusUpdateActorModule

import com.vaadin.ui.AbstractLayout
import com.vaadin.ui.HorizontalLayout
import com.vaadin.ui.Label
import com.vaadin.ui.TextField
import com.vaadin.ui.VerticalLayout

final class EnvironmentStatusbar(component: AbstractScalaPrototypeComponent)
    extends AbstractScalaPrototypeComponent(component, new VerticalLayout) {

  override def defineWithin(container: AbstractLayout) =
    {
      require(null != container)
      val platformInfo: String = composePlatformInfo
      val txtfPlatform: TextField = new TextField()
      txtfPlatform.setValue(platformInfo)
      txtfPlatform.setEnabled(false)
      val labelReady = new Label("Ready")
      val statusBar = new HorizontalLayout()
      statusBar.addComponents(labelReady)
      val txtfHeapStts: AbstractUpdatableTextField = new HeapStatusTextField(statusBar)
      statusBar.addComponents(txtfPlatform)
      txtfPlatform.setSizeFull;
      statusBar.setSizeFull;
      statusBar.setExpandRatio(labelReady, 5.0f);
      statusBar.setExpandRatio(txtfPlatform, 70.0f);
      setSizeFull;
      ScalaComponentOpers.addComponents(container, statusBar)
      new HeapStatusUpdateActorModule(txtfHeapStts).execute
    }

  override def reset: Unit = {
    //TODO
  }

  private def composePlatformInfo: String = {
    val strBldr = new StringBuilder()
    strBldr.append(System.getProperty("os.name")).append(" (").append(System.getProperty("os.version")).append(") \t ")
      .append(System.getProperty("os.arch").replaceAll("amd", "").concat(" bit"))
      .append(" \t ").append(Runtime.getRuntime().availableProcessors())
      .append(" Cores \t").append(Runtime.getRuntime().getClass().getPackage().getImplementationTitle())
      .append(" - ").append(System.getProperty("java.version")).append(" \t ").append(System.getProperty("java.vendor"))
      .append(" \t Language ").append(System.getProperty("user.language"))
    return strBldr.toString()
  }
}