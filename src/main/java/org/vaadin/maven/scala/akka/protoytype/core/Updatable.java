package org.vaadin.maven.scala.akka.protoytype.core;

public interface Updatable
{
    void update(final String newValue);
}
