package org.vaadin.maven.scala.akka.protoytype.foundation;

import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.IllegalFormatException;
import java.util.Locale;

import org.slf4j.LoggerFactory;

public final class HeapStatusSimpleProvider
{
    private HeapStatusSimpleProvider()
    {
        super();
    }

    public static String provideHeapStatus()
    {
        return composeHeapStatus(deriveMemoryStatus());
    }

    private static String composeHeapStatus(final long... memoryStatus)
    {
        final StringBuilder strBldr = new StringBuilder();
        strBldr.append("%d M of %d M"); //NOPMD
        String output = Constants.EMPTY; //NOPMD
        try (final Formatter formatter = new Formatter(Locale.UK);)
        {
            formatter.format(strBldr.toString(), memoryStatus[0], memoryStatus[1]); //NOPMD
            output = formatter.toString(); //NOPMD
        }
        catch (IllegalFormatException | FormatterClosedException e)
        {
            LoggerFactory.getLogger(HeapStatusSimpleProvider.class).error("Error while composing Heap status ", e); //NOPMD
        }
        return output;
    }

    private static long deriveMemoryStatus()[]
    {
        final long freeMemoryMiB = Runtime.getRuntime().freeMemory() / 1000000; //NOPMD
        final long totalMemorMiB = Runtime.getRuntime().totalMemory() / 1000000; //NOPMD
        final long memoryInUse = totalMemorMiB - freeMemoryMiB;
        return new long[] {memoryInUse, totalMemorMiB };
    }
}
