package org.vaadin.maven.scala.akka.protoytype.view;

import org.vaadin.maven.scala.akka.protoytype.core.Updatable;

import com.vaadin.ui.AbstractOrderedLayout;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;

abstract class AbstractUpdatableTextField implements Updatable
{
    private final AbstractTextField txtField;
    private final AbstractOrderedLayout vaadinContainer;

    protected AbstractUpdatableTextField(final AbstractOrderedLayout container)
    {
        super();
        assert null != container : "Parameter 'container' of 'AbstractUpdatableTextField''s ctor must not be null";

        txtField = new TextField();
        this.vaadinContainer = container;
        this.vaadinContainer.addComponents(this.txtField);
        this.txtField.setEnabled(false);
        this.vaadinContainer.setComponentAlignment(this.txtField, Alignment.MIDDLE_CENTER);
        this.vaadinContainer.setExpandRatio(this.txtField, 20.0f);
    }

    protected abstract void setExpandRatio();

    @Override
    public final void update(final String newValue)
    {
        assert null != newValue && !newValue.isEmpty() : "Parameter 'newValue' of method 'update' must not be empty";

        if (UI.getCurrent().isAttached())
        {
            UI.getCurrent().access(() -> {
                this.getTextField().setValue(newValue);
            });
        }
    }

    protected final AbstractTextField getTextField()
    {
        return this.txtField;
    }

    protected final void setCaption(final String caption)
    {
        assert null != caption && !caption.isEmpty() : "Parameter 'caption' of method 'setTextFieldCaption' must not be empty";

        this.txtField.setCaption(caption);
    }

    protected final void setValue(final String value)
    {
        assert null != value && !value.isEmpty() : "Parameter 'value' of method 'setTextFieldValue' must not be empty";

        this.txtField.setValue(value);
    }

    protected final AbstractOrderedLayout getVaadinContainer()
    {
        return this.vaadinContainer;
    }
}
