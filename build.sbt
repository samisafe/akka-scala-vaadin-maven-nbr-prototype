import org.vaadin.sbt.VaadinPlugin._
import sbt.Keys._
import sbt.ScalaVersion

name := """akka-scala-vaadin-maven-nbr-prototype"""

version := "1.0.0"

scalaVersion := "2.11.7"

crossPaths := false

autoScalaLibrary := false

javacOptions ++= Seq("-source", "1.8", "-target", "1.8")

enablePlugins(JettyPlugin)

resolvers ++=Seq(
	  "Vaadin Snapshots" at "http:/oss.sonatype.org/content/repositories/vaadin-snapshots/", 
	  "Vaadin add-ons" at "http://maven.vaadin.com/vaadin-addons")

libraryDependencies ++= Seq (
  "com.typesafe.akka" % "akka-actor_2.11" % "2.3.12", 
  "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided" 
)

lazy val root = project.in(file(".")).aggregate(deps, conf)

lazy val deps = project.settings(vaadinAddOnSettings :_*).settings(
  name := "Prototype",
  libraryDependencies := Dependencies.addonDeps,
  // Javadoc generation causes problems so disabling it for now
  mappings in packageVaadinDirectoryZip <<= (packageSrc in Compile) map {
    (src) => Seq((src, src.name))
  },
  sources in doc in Compile := List()
)

lazy val conf = 
  project.enablePlugins(JettyPlugin).settings(vaadinWebSettings :_*).settings(
    name := "prototype",
    artifactName := { 
      (sv: ScalaVersion, module: ModuleID, artifact: Artifact) => "Prototype." + artifact.extension },
      libraryDependencies ++= Dependencies.actualDeps,
      javaOptions in compileVaadinWidgetsets := Seq("-Xss8M", "-Xmx512M", "-XX:MaxPermSize=512M"),
      vaadinOptions in compileVaadinWidgetsets := Seq("-strict", "-draftCompile"),
      skip in compileVaadinWidgetsets in resourceGenerators := true,
      javaOptions in vaadinDevMode ++= Seq("-Xdebug", "-Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005"),
      // JavaDoc generation causes problems
      sources in doc in Compile := List()
).dependsOn(deps)


fork in run := true